#include <sstream>
#include <cstdio>
#include "system.hpp"

using std::string;
using std::stringstream;


int execute_command(string const & cmd, string & out)
{
	FILE * in;
	char buff[4096];
	if (!(in = popen(cmd.c_str(), "r")))
		return 1;

	stringstream sout;
	while(fgets(buff, sizeof(buff), in))
		sout << buff;

	out = sout.str();
	int exit_code = pclose(in);

	return exit_code;
}

std::string filename(int fno)  
{
	std::string desc = "/proc/self/fd/" + std::to_string(fno);
	char buf[4096];
	size_t len = readlink(desc.c_str(), buf, 4096);
	buf[std::min(len, 4095ul)] = '\0';
	return buf;
}

std::string filename(FILE * f)
{
	return filename(fileno(f));
}
