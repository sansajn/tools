#pragma once
#include <string>
#include <unistd.h>

/*! \return on success, returns the exit status of the command; if
wait4(2) returns an error, or some other error is detected, -1 is
returned. Function set errno to an appropriate value in the case of
an error. */
int execute_command(std::string const & cmd, std::string & out);

//! \param fno file number obtained by fileno() function
std::string filename(int fno);

std::string filename(FILE * f);
