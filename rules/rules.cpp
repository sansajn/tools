#include "rules.hpp"

using std::any_of;
using std::none_of;

rules_engine::rules_engine()
	: Not{*this}
{}

void rules_engine::If(bool cond)
{
	_sufficient.push_back(cond);
}

bool rules_engine::operator()() const
{
	auto is_true = [](bool b){return b;};
	return any_of(begin(_sufficient), end(_sufficient), is_true)
		&& none_of(begin(_preventing), end(_preventing), is_true);
}

rules_engine::preventing_rules_engine::preventing_rules_engine(rules_engine & re)
	: _re{re}
{}

void rules_engine::preventing_rules_engine::If(bool cond)
{
	_re._preventing.push_back(cond);
}
