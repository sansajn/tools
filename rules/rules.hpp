#include <algorithm>
#include <vector>

/*! rules engine implementation
TODO: create more usable API and find a better names */
class rules_engine
{
public:
	rules_engine();
	void If(bool cond);

	// NotIf logic
	class preventing_rules_engine
	{
	public:
		preventing_rules_engine(rules_engine & re);
		void If(bool cond);

	private:
		rules_engine & _re;
	};

	preventing_rules_engine Not;

	bool operator()() const;

private:
	std::vector<bool> _sufficient, _preventing;
};
