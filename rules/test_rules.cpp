#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include "rules.hpp"
using std::find;
using std::vector;
using std::string;
using std::cout;


class customer
{
public:
	customer(string const & name, double value, bool something_returned, bool defaulted)
		: _name{name}, _val{value}, _returned{something_returned}, _defaulted{defaulted}
	{}

	double purchased_goods_value() const {return _val;}
	bool returned_items() const {return _returned;}
	bool defaulted() const {return _defaulted;}
	bool operator==(customer const & rhs) const {return _name == rhs._name;}

private:
	string _name;
	double _val;
	bool _returned;
	bool _defaulted;
};

int main(int argc, char * argv[])
{
	vector<customer> survey_responders{customer{"john", 1500.0, false, true}};

	customer const & john = survey_responders[0];

	const bool isAGoodCustomer = john.purchased_goods_value() >= 1000
			|| !john.returned_items()
			|| find(begin(survey_responders), end(survey_responders), john) != end(survey_responders);

	if (isAGoodCustomer)
		cout << "good customer\n";
	else
		cout << "bad customer\n";


	{
		rules_engine good_customer;
		good_customer.If(john.purchased_goods_value() >= 1000);
		good_customer.If(!john.returned_items());
		good_customer.If(find(begin(survey_responders), end(survey_responders), john) != end(survey_responders));

		auto bad_customer = good_customer.Not;
		bad_customer.If(john.defaulted());

		if (good_customer())
			cout << "good customer\n";
		else
			cout << "bad customer\n";
	}

	return 0;
}
