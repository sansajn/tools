// observer sample
#include <iostream>
#include "observer.hpp"

using std::find;
using std::vector;
using std::cout;


struct a_listener
{
	virtual void a() = 0;
};

struct b_listener
{
	virtual void b() = 0;
};

struct single_listener_source : public observable_with<a_listener>
{
	void fire_a()
	{
		notify_listener(&a_listener::a);
	}
};

struct single_listener_impl : public a_listener
{
	single_listener_impl(single_listener_source & s)
		: _src{s}
	{
		_src.register_listener(this);
	}

	~single_listener_impl()
	{
		_src.forget_listener(this);
	}

	void a() override
	{
		cout << "simgle_listener::a()" << std::endl;
	}

private:
	single_listener_source & _src;
};

struct multiple_listener_source
	: public observable_with<a_listener>, public observable_with<b_listener>
{
	void fire_a()
	{
		::notify_listener(&a_listener::a, *this);
	}

	void fire_b()
	{
		::notify_listener(&b_listener::b, *this);
	}
};

struct multiple_listener_impl : public a_listener, public b_listener
{
	multiple_listener_impl(multiple_listener_source & s)
		: _src{s}
	{
		::register_listener<a_listener>(_src, this);
		::register_listener<b_listener>(_src, this);
	}

	~multiple_listener_impl()
	{
		::forget_listener<a_listener>(_src, this);
		::forget_listener<b_listener>(_src, this);
	}

	void a() override
	{
		cout << "multiple_listener_impl::a()" << std::endl;
	}

	void b() override
	{
		cout << "multiple_listener_impl::b()" << std::endl;
	}

private:
	multiple_listener_source & _src;
};

int main(int argc, char *argv[])
{
	single_listener_source s1;
	single_listener_impl i1{s1};
	s1.fire_a();  // simgle_listener::a()

	multiple_listener_source s2;
	multiple_listener_impl i2{s2};
	s2.fire_a();  // multiple_listener_impl::a()
	s2.fire_b();  // multiple_listener_impl::b()

	return 0;
}
