#pragma once
#include <vector>
#include <algorithm>

template <typename L>
class observable_with
{
public:
	void register_listener(L * listener);
	void forget_listener(L * listener);

	template <typename Ret, typename ... Args>
	void notify_listener(Ret L::* p, Args ... args);

	std::vector<L *> const & listeners() const;

private:
	std::vector<L *> _listeners;
};

template <typename L>
void observable_with<L>::register_listener(L * listener)
{
	if (std::find(_listeners.begin(), _listeners.end(), listener) == _listeners.end())
		_listeners.push_back(listener);
}

template <typename L>
void observable_with<L>::forget_listener(L * listener)
{
	auto it = std::find(_listeners.begin(), _listeners.end(), listener);
	if (it != _listeners.end())
		_listeners.erase(it);
}

template <typename L>
template <typename Ret, typename ... Args>
void observable_with<L>::notify_listener(Ret L::* p, Args ... args)
{
	for (L * l : _listeners)
		(l->*p)(args ...);
}

template <typename L>
std::vector<L *> const & observable_with<L>::listeners() const
{
	return _listeners;
}


//! multiple listener source helpers
//@{

template <typename Listener, typename Source, typename ListenerImpl>
void register_listener(Source & src, ListenerImpl * impl)
{
	src.observable_with<Listener>::register_listener(impl);
}

template <typename Listener, typename Source, typename ListenerImpl>
void forget_listener(Source & src, ListenerImpl * impl)
{
	src.observable_with<Listener>::forget_listener(impl);
}

template <typename Ret, typename Listener, typename Source, typename ... Args>
void notify_listener(Ret Listener::*p, Source & src, Args ... args)
{
	src.observable_with<Listener>::notify_listener(p, args ...);
}

//@}
