/*! immutable inode attribute support (only for linux) */
#pragma once
#include "path.hpp"

namespace fs {

bool is_immutable(fs::path const & p);

//! set/unset inode immutable attribute
void immutable_attr(fs::path const & p, bool flag);

}  // fs
