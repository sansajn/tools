// weakly_canonical eqvivalent for older boost (<1.62) versions
#include "path.hpp"

namespace fs {

path weakly_canonical(path const & p);

}  // fs
