#include "immutable.hpp"
#include <cassert>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/fs.h>

namespace fs {

bool is_immutable(fs::path const & p)
{
	int fd = open(p.string().c_str(), O_RDONLY);
	assert(fd != 0);  // file not found

	int attrs = 0;
	ioctl(fd, FS_IOC_GETFLAGS, &attrs);

	close(fd);

	return attrs & FS_IMMUTABLE_FL;
}

void immutable_attr(fs::path const & p, bool flag)
{
	int fd = open(p.string().c_str(), O_RDONLY);
	assert(fd != 0);  // file not found

	int attrs = 0;
	ioctl(fd, FS_IOC_GETFLAGS, &attrs);

	if (!(attrs & FS_IMMUTABLE_FL))
	{
		attrs |= FS_IMMUTABLE_FL;
		ioctl(fd, FS_IOC_SETFLAGS, &attrs);
	}

	close(fd);
}

}  // fs
