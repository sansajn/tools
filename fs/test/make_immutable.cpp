#include <string>
#include <iostream>
#include "../immutable.hpp"

using std::string;
using std::cout;

int main(int argc, char * argv[])
{
	fs::path ifile = argc > 1 ? argv[1] : "immutable.txt";

	fs::immutable_attr(ifile, true);  // make file immutable

	cout << "file " << ifile << " should be now immutable, check with lsattr command\n"
		<< "done!\n";

	return 0;
}
