#include <vector>
#include <utility>
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "../weakly_canonical.hpp"

using std::vector;
using std::pair;
using fs::path;


TEST_CASE("dots and double dots must be removed",
	"[weakly_canonical]")
{
	vector<path> input_paths = {"/home/adam/../test", "/home/adam/../test/./.."};
	vector<path> expected_results = {"/home/test", "/home"};

	for (size_t i = 0; i < input_paths.size(); ++i)
		REQUIRE(fs::weakly_canonical(input_paths[i]) == expected_results[i]);
}

TEST_CASE("edge case tests",
	"[weakly_canonical]")
{
	REQUIRE_THROWS(fs::weakly_canonical("../home"));
	REQUIRE_THROWS(fs::weakly_canonical("/home/../.././../"));
}
