// checks if specified file is immutable or not
#include <string>
#include <iostream>
#include "../immutable.hpp"

using std::string;
using std::cout;

int main(int argc, char * argv[])
{
	fs::path ifile = argc > 1 ? argv[1] : "immutable.txt";

	if (fs::is_immutable(ifile))
		cout << "file " << ifile << " is immutable\n";
	else
		cout << "file " << ifile << " is not immutable\n";

	cout << "done!\n";
	return 0;
}
