#include <deque>
#include <boost/filesystem/operations.hpp>
#include "weakly_canonical.hpp"

namespace fs {

using std::deque;
using boost::filesystem::filesystem_error;
using boost::system::error_code;
using boost::system::generic_category;

path weakly_canonical(path const & p)
{
	deque<path> s;

	// decompose
	for (path const & elem : p)
	{
		if (elem == "..")
		{
			if (!s.empty())
				s.pop_back();
			else
				throw filesystem_error{"unable to canonize '" + p.string() + "' path",
					error_code{1, generic_category()}};
		}
		else if (elem != ".")
			s.push_back(elem);
	}

	// reconstruct
	path result;
	for (path const & p : s)
		result /= p;

	return result;
}

}  // fs
