**OBSAH**

[TOC]


# rozsahy (range/)

základna podpora pre rozsahy, knižnica obsahuje nasledujúce moduly

**filter** : implementuje podporu pre filtrovanie rozsahou (jednotlivé položky rozsahu nie je možné meniť)

```c++
vector<int> data = vector<int>{{1, 2, 3, 4, 5, 6}};
auto only_even = filtered(data, [](int n){return (n%2) == 0;});
```

**printer** : podpora pre vyzualizáciu rozsahou

```c++
vector<int> data = vector<int>{{1, 2, 3, 4, 5, 6}};
auto only_even = filtered(data, [](int n){return (n%2) == 0;});
cout << "only_even=" << printable(only_even) << "\n";
```

---


# concurrency/

Modul implementuje thread safe štruktúry ako napr. `concurrent_queue`, ... . Nasleduje typická ukážka producent-konzument v ktorej hlavné vlákno (producent) vytvára dáta pre pomocné vlákno (konzument), ktoré dáta ďalej spracuje (v našom prípade zobrazí na obrazovku).


```c++
concurrent_queue<int> q;
std::thread w{[&q]{
	int i;
	while (q.wait_and_pop(i, std::chrono::seconds{3}))
		cout << i << std::endl;
	cout << "done." << std::endl;
}};

q.push(1);
q.push(4);
q.push(2);
q.push(3);

w.join();
```

---


# čítanie zo súboru (file_view/)

Modul umožnuje čítanie zo súboru, čítanie do pamäti implemetnuje funkcia `io::read_file()` a riadkovo orientované čítanie funkcia `io::read_lines()`. 

Prvá ukážka spočíta počet riadkou súboru `test.txt` pomocou riadkovo orientovaného čítania.

```c++
int count = 0;
for (string const & line : io::read_lines("test.txt"))
	count += 1;
cout << "we have " << count << " number lines of code in 'text.txt' file\n";
```

Druhá ukážka spočíta počet znakou v súbore.

```c++
string content = io::read_file("test.txt");
cout << "we have " << size(content) << " characters in 'test.txt' file\n";
```

---


# rules (rules/)

**rules_engine** : umožnuje deklaratývnym spôsobom vytvárať a vyhodnocovať sady pravidiel, implementované na základe článku
[Declarative If Statements with a Simplified Rules
Engine](https://www.fluentcpp.com/2019/01/18/a-simplified-rules-engine-to-make-declarative-if-statements/).

```c++
for (customer const & john : customers)
{
	rules_engine good_customer;
	good_customer.If(john.purchased_goods_value() > 1000);
	good_customer.If(!john.returned_items());
	good_customer.If(find(survey_responders, john) != end(survey_responders));

	preventing_rules_engine not_good_customer = good_customer.Not;
	not_good_customer.If(john.defaulted());

	if (good_customer())
		cout << "good customer found\n";
	else
		cout << "not good customer found\n";
}

```

>TODO: nájdi slovenský ekvivalent pre rules engine

---

# konfigurácia (config/)

podpora pre čítanie konfigurákou, modul obsahuje podporu pre konfiguračné
súbory v JSON formáte a lua skripte.

Napr. prečítať konfiguračný súbor v jazyku lua môžme takto:

```c++
#include "config/lua_config.hpp"

struct app_conf
{
	string python_home;
	string format;
	string bitrate;
	int freq;
	bool verbose;

	void read(string const & fname)
	{
		lua_config conf{fname};
		python_home = conf.get("python_home");
		format = conf.get("format");
		bitrate = conf.get("bitrate");
		freq = conf.get<int>("freq");
		verbose = conf.get<bool>("verbose");
	}
};

int main(int argc, char * argv[])
{
	app_conf conf;
	conf.read("config.lua");
	cout << "bitrate=" << conf.bitrate << "\n";
}
```

konfiguračný súbor `config.lua`

```lua
python_home='python27'
ffmpeg_home='ffmpeg'  -- ffmpeg player binary
format='mp3'
bitrate='192k'
freq=48000
verbose=true
```

---

# signáli (event/)

podpora pre asynchrónne udalosti (signal/slot system)

```c++
void foo()
{
	cout << "foo()" << std::endl;
}

void test_event()
{
	event_handler<> h{foo};
	event<> e;
	e.add(h);
	e.call();
	e.remove(h);
}
```

---

# pozorovateľ (observer/)

Modul implementuje triedu `observable_with` pomocou ktorej je možné jednoducho implementovať návrhový vzor pozorovateľ.

```c++
#include <iostream>
#include "observer.hpp"

struct a_listener {
	virtual void a() = 0;
};

struct event_source : observable_with<a_listener> {
	void fire_a() {
		notify_listener(&a_listener, a);
	}
};

struct listener_impl : public a_listener {
	void a() override {
		cout << "listener_impl::a()" << std::endl;	
	}
};

int main(int argc, char * argv[]) {
	event_source src;
	listener_impl listener;
	src.register_listener(&listener);
	src.fire_a();  // listener_impl::a()
	return 0;
}
```

---

# sqlite3 (sql3/)

modul uľahčujúci prácu s sqlite3 databázou

```c++
#include "sql3/session.hpp"

void test_sql()
{
	dbw::session sql("test.db");

	std::vector<int> one;
	std::vector<double> two;
	std::string tree;

	sql << "select one, two, tree from test where one=" << 1, one, two, tree;
}
```

---

# súborový sytém (fs/)

Modul rozširujúci boost::filesystem, zahŕňa `boost::filesystem` v mennom prestore `fs`, pridáva napr. `is_immutable`, `immutable_attr` pre prácu s immutable atribútom pod linuxom, alebo aj implementáciu `weakly_canonical` pre staršie verzie boost-u.

```c++
#include "fs.hpp"

void test_fs()
{
	cout
		<< "temp: " << fs::standard_directories::temp() << "\n"
		<< "user home: " << fs::standard_directories::user_home() << "\n"
		<< "current working: " << fs::standard_directories::working() << "\n"
		<< "program directory: " << fs::standard_directories::program_directory() << "\n";
}
```

---

# fake_boost (fake_boost/)

**directory_iterator** : Umožnuje iterovať obsah adresára v posix kompatibilnom
systéme. Iterácia pomocou štandardneho for cykla

```c++
directory_iterator it{path_to_list}, end_it;
for (; it != end_it; ++it)
	cout << (*it)->d_name << "\n";
```

, alebo pomocou for-loop s c++11

```c++
for (auto entry : directory_iterator{path_to_list})
	cout << entry->d_name << "\n";
```


**boost::filesystem::path** : Implementuje cestu súboru/adresára v súborovom
systéme.

```c++
path p{"/foo/bar.txt"};
// ...
```

, pozri `test_path.cpp` pre ďalšie ukážky použitia.


**boost.filesystem.operations** : Implementované funkcie s modulu operations

```c++
exists()
file_size()
```

Ukážka:

```c++
path p{"foo/bar.txt"};
if (exists(p))
	// do something ...
```

, pozri 'test_operations.cpp' pre ďalšie ukážky použitia.


# systém (system/)

Modul implementuje funkciu execute_command() pomocou ktorej je možné spúštať shell príkazy, napr.

```c++
string result;
if (execute_command("lsof -p " + to_string(getpid()) + "|wc -l", result) == 0)
{
	trim(result);
	int descriptor_count = boost::lexical_cast<int>(result);
	// ...
}
```

ukážka zistí počet súborových deskriptorou otvorených procesom, spustením príkazu `lsof -p PID|wc -l`.


# json (json/)

Podpora pre prácu s JSON súbormi založená na `boost::property_tree` knižnici. Napr. čítanie poľa

```c++
string array_json = R"({
	"numbers": ["one", "two", "three"]
})";

jtree root;
read_json_from_memory(array_json, root);

vector<string> numbers;
get(root, "numbers", numbers);
```



Author: Adam Hlavatovič, adam.hlavatovic@protonmail.ch

