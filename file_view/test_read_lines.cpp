#include <string>
#include <iostream>
#include "read_lines.hpp"

using std::string;
using std::cout;

int main(int argc, char * argv[])
{
	string input = argc > 1 ? argv[1] : "SConstruct";

	int count = 0;
	for (auto const & line : io::read_lines(input))
		count += 1;

	cout << "we have " << count << " number of lines of code in '" << input << "' file" << std::endl;
	return 0;
}
