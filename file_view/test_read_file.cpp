#include <algorithm>
#include <iostream>
#include "read_file.hpp"

using std::string;
using std::for_each;
using std::cout;

int main(int argc, char * argv[])
{
	string input = argc > 1 ? argv[1] : "SConstruct";
	
	string content = io::read_file(input);
	int count = 0;
	for (auto const & ch : content)
		if (ch == '\n')
			count += 1;
		
	cout << "we have " << count << " number of lines of code in '" << input << "' file" << std::endl;
	return 0;
}
