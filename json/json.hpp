//! boost::property_tree based JSON support
#pragma once
#include <string>
#include <vector>
#include <boost/property_tree/json_parser.hpp>

using jtree = boost::property_tree::ptree;
using jtree_error = boost::property_tree::ptree_error;

using boost::property_tree::read_json;

void read_json_from_memory(std::string const & data, jtree & json);

namespace detail {

template <typename T>
void read_array(jtree const & node, std::vector<T> & result)
{
	if (!node.data().empty())
		throw jtree_error{"unable to parse array object, reason '" + node.data() + "' is not an array"};

	result.reserve(node.size());
	for (jtree::value_type const & keyval : node)
		result.push_back(keyval.second.get_value<T>());
}

}  // detail

//! get array
template <typename T>
void get(jtree const & root, std::string const & key, std::vector<T> & result)
{
	jtree const & arr = root.get_child(key);
	detail::read_array(arr, result);
}

template <typename T>
void get_optional(jtree const & root, std::string const & key, std::vector<T> & result)
{
	boost::optional<jtree const &> arr = root.get_child_optional(key);
	if (arr)
		detail::read_array(*arr, result);
}
