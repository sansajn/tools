#include <string>
#include <vector>
#include <catch.hpp>
#include "../json.hpp"

using std::string;
using std::vector;

TEST_CASE("read JSON array",
	"[get][read]")
{
	vector<string> expected_results = {"one", "two", "three"};

	string array_json = R"({
		"numbers": ["one", "two", "three"]
	})";

	jtree root;
	read_json_from_memory(array_json, root);

	vector<string> numbers;
	get(root, "numbers", numbers);

	REQUIRE(numbers == expected_results);
}
