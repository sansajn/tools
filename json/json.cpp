#include <sstream>
#include "json.hpp"

using std::stringstream;

void read_json_from_memory(std::string const & data, jtree & root)
{
	stringstream ss{data};
	read_json(ss, root);
}
