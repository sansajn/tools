#include <chrono>
#include <thread>
#include <iostream>
#include "concurrent_queue.hpp"

using std::cout;

int main(int argc, char * argv[])
{
	concurrent_queue<int> q;
	q.push(1);
	q.push(4);
	q.push(2);
	q.push(3);

	std::thread w{[&q]{
		int i;
		while (q.wait_and_pop(i, std::chrono::seconds{3}))
			cout << i << std::endl;
		cout << "worker done!" << std::endl;
	}};
	
	q.push(9);
	q.push(6);
	q.push(8);
	q.push(5);
	q.push(7);

	w.join();

	cout << "done!\n";

	return 0;
}
