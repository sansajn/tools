#include <chrono>
#include <thread>
#include <iostream>
#include "concurrent_queue.hpp"

using std::cout;

int main(int argc, char * argv[])
{
	concurrent_queue<int> q;

	std::thread w{[&q]{
		int i;
		while (q.wait_and_pop(i, std::chrono::seconds{3}))
			cout << i << std::endl;
		cout << "worker done!" << std::endl;
	}};
	
	q.cancel();
	
	w.join();

	cout << "done!\n";

	return 0;
}
